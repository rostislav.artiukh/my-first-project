define("UsrConcertProgram1Page", ["UsrConstantConcert"], function(UsrConstantConcert) {
return {
entitySchemaName: "UsrConcertProgram",
"attributes": {
	CountRowsValue: {
	dataValueType: Terrasoft.DataValueType.INTEGER,
	value: 0
	},
	SystemValue: {
	dataValueType: Terrasoft.DataValueType.INTEGER,
	value: 0
	}
},
modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
details: /**SCHEMA_DETAILS*/{
	"Files": {
		"schemaName": "FileDetailV2",
		"entitySchemaName": "UsrConcertProgramFile",
		"filter": {
			"masterColumn": "Id",
			"detailColumn": "UsrConcertProgram"
		}
	},
	"UsrSchema78451129Detail4111fce1": {
		"schemaName": "UsrSchema78451129Detail",
		"entitySchemaName": "UsrPerfomance",
		"filter": {
			"detailColumn": "UsrIdConcert",
			"masterColumn": "Id"
		}
	}
}/**SCHEMA_DETAILS*/,
businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
methods: {
	doSqlQuery: function(){
		// получение и назначение системной настройки
        	this.Terrasoft.SysSettings.querySysSettingsItem("MaxCountActiveEveryDayConcertProgramm", function(value) {
            this.set("SystemValue", value);                            
            }, this);
        var sysSetting = this.get("SystemValue");
        var countRows = 0;

        // создам схему Ентити квери
           var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
		    rootSchemaName: "UsrConcertProgram"
		});
		
			esq.addColumn("UsrName", "UsrName");
		
		var esqActiveFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "UsrBoolean1",
		true);
		var esqPeriodFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "UsrLookup2.Id",
		UsrConstantConcert.Order.OrderStatus.InPlanned);
		var esqIdFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.NOT_EQUAL, "Id",
		this.get("Id"));
		
			esq.filters.add("esqActiveFilter", esqActiveFilter);
			esq.filters.add("esqPeriodFilter", esqPeriodFilter);
			esq.filters.add("esqIdFilter", esqIdFilter);
			
			esq.addAggregationSchemaColumn("UsrName", Terrasoft.AggregationType.COUNT, "UniqueActivitiesCount",
		Terrasoft.AggregationEvalType);
			esq.getEntityCollection(function (result) {
		    if (!result.success) {
		        // обработка/логирование ошибки, например
		        this.showInformationDialog("Ошибка запроса данных");
		        return;
		    }
	  countRows = result.collection.getCount();
	  if(countRows === sysSetting){
	  this.set("CountRowsValue", countRows);
	  //this.showInformationDialog(this.get("MyCompletenessValue"));
	  		}			  
		}, this);
		},
	
    	dueDateValidator: function() {
        // проверка на значение словаря ежедневно и на активность
        var invalidMessage = "";
        if(this.get("UsrLookup2") !== undefined){
        if(this.get("UsrLookup2").value === UsrConstantConcert.Order.OrderStatus.InPlanned &&
        this.get("UsrBoolean1") &&  this.get("CountRowsValue") >= this.get("SystemValue")) {
        	 return {
            invalidMessage: "свободных концертных залов мало и допускается не более " + this.get("SystemValue") 
            + "программ"
        		};
        	}
        }	
             return {
            // Сообщение об ошибке валидации.
            invalidMessage: invalidMessage
        		};	
    	},
    	onEntityInitialized: function() {
		this.callParent(arguments);
		this.doSqlQuery();
		this.dueDateValidator();
	},
    // Переопределение базового метода, инициализирующего пользовательские валидаторы.
    setValidationConfig: function() {
        // Вызывает инициализацию валидаторов родительской модели представления.
        this.callParent(arguments);
        // Для колонки [DueDate] добавляется метод-валидатор dueDateValidator().
        this.addColumnValidator("UsrBoolean1", this.dueDateValidator);
    }
},
	dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
	diff: /**SCHEMA_DIFF*/[
		{
			"operation": "insert",
			"name": "UsrNameb1d2a2b0-d41f-455c-9c0e-1ccdfd9cb068",
			"values": {
				"layout": {
					"colSpan": 24,
					"rowSpan": 1,
					"column": 0,
					"row": 0,
					"layoutName": "ProfileContainer"
				},
				"bindTo": "UsrName"
			},
			"parentName": "ProfileContainer",
			"propertyName": "items",
			"index": 0
		},
		{
			"operation": "insert",
			"name": "LOOKUPba64e85b-e729-4151-8204-e96213d82571",
			"values": {
				"layout": {
					"colSpan": 24,
					"rowSpan": 1,
					"column": 0,
					"row": 1,
					"layoutName": "ProfileContainer"
				},
				"bindTo": "UsrLookup1",
				"enabled": true,
				"contentType": 3
			},
			"parentName": "ProfileContainer",
			"propertyName": "items",
			"index": 1
		},
		{
			"operation": "insert",
			"name": "INTEGER6d8baa05-fb4e-443f-9052-5cc433975120",
			"values": {
				"layout": {
					"colSpan": 12,
					"rowSpan": 1,
					"column": 0,
					"row": 0,
					"layoutName": "Header"
				},
				"bindTo": "UsrInteger1",
				"enabled": true
			},
			"parentName": "Header",
			"propertyName": "items",
			"index": 0
		},
		{
			"operation": "insert",
			"name": "LOOKUPb983d26b-8748-432a-a744-cb7547662f65",
			"values": {
				"layout": {
					"colSpan": 12,
					"rowSpan": 1,
					"column": 12,
					"row": 0,
					"layoutName": "Header"
				},
				"bindTo": "UsrLookup2",
				"enabled": true,
				"contentType": 3
			},
			"parentName": "Header",
			"propertyName": "items",
			"index": 1
		},
		{
			"operation": "insert",
			"name": "STRING0f5aa2ef-5b2e-4e58-8a7d-9839e0a3f4a3",
			"values": {
				"layout": {
					"colSpan": 12,
					"rowSpan": 1,
					"column": 0,
					"row": 1,
					"layoutName": "Header"
				},
				"bindTo": "UsrString2",
				"enabled": true
			},
			"parentName": "Header",
			"propertyName": "items",
			"index": 2
		},
		{
			"operation": "insert",
			"name": "STRINGab30eed9-0a85-43c3-8b48-bb977c738b30",
			"values": {
				"layout": {
					"colSpan": 12,
					"rowSpan": 1,
					"column": 12,
					"row": 1,
					"layoutName": "Header"
				},
				"bindTo": "UsrString3",
				"enabled": true
			},
			"parentName": "Header",
			"propertyName": "items",
			"index": 3
		},
		{
			"operation": "insert",
			"name": "BOOLEAN2039661f-8c22-4270-b1e0-146a3bbf847d",
			"values": {
				"layout": {
					"colSpan": 24,
					"rowSpan": 1,
					"column": 0,
					"row": 2,
					"layoutName": "Header"
				},
				"bindTo": "UsrBoolean1",
				"enabled": true
			},
			"parentName": "Header",
			"propertyName": "items",
			"index": 4
		},
		{
			"operation": "insert",
			"name": "Tabd0b27a2cTabLabel",
			"values": {
				"caption": {
					"bindTo": "Resources.Strings.Tabd0b27a2cTabLabelTabCaption"
				},
				"items": [],
				"order": 0
			},
			"parentName": "Tabs",
			"propertyName": "tabs",
			"index": 0
		},
		{
			"operation": "insert",
			"name": "UsrSchema78451129Detail4111fce1",
			"values": {
				"itemType": 2,
				"markerValue": "added-detail"
			},
			"parentName": "Tabd0b27a2cTabLabel",
			"propertyName": "items",
			"index": 0
		}
	]/**SCHEMA_DIFF*/
};
});
